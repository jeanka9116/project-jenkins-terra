variable "AWS_REGION" {
  default = "us-east-2"
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "mykey"
}

variable "PATH_TO_PUBLIC_KEY" {
  default = "mykey.pub"
}

variable "INSTANCE_DEVICE_NAME" {
  default = "/dev/xvdh"
}

variable "JENKINS_VERSION" {
  default = "2.204.5"
}

variable "TERRAFORM_VERSION" {
  default = "0.12.23"
}

variable "APP_INSTANCE_COUNT" {
  default = "0"
}

variable "DUMMY_SSH_PUB_KEY" {
  description = "public ssh key to put in place if there's no public key defined - to avoid errors in jenkins if it doesn't have a public key"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC8WnpFGry2yrt4gJkBlPkHcpBDw472Y14FVyAZ/N1zzimBKHE1jvU8+Sc81DXO2ev+OLc2zp4L23et5du8ocbR91riAlYygkhxS4ACI0wtZ3JH9Jt+ew/64W2i7hLrBb0Jqmfq/vrG9tvvwpXFrBerjMnsdZVm19b40wWcaLRI39sBqMGmQTotW7Ta1LwdQt2DPtglJ+9yAI2InloQPFTh4tt6b6l9Yh413m6zyVtobZIntcmi+o9bNHq1KzConQJHWFB6hX/igefgbXFheV/yUNhdPrfWZWRXwETh1+zvRvI1T7SNBrHLjmerGvrhn4xz/X42FRip6zwn1h80oyvjG7liQPlXJkRTU6DStRLff7UpPBpDgrvVEgy+MvvRGc7izQqWYTlSo/m+vajjbod/EPp2YGU7StG680pSgYig/sl30J8tlKDJiLxdf+zZFaZySJ1PE7sKqrcqU0UfTaVaSxeiXpFFeFnmweMq98B1igYp0sWdDfMG+IRL8ThO3c0= root@jean-B450M-DS3H"
}
